import {Headmap} from './heatmap';
import {IAnalyseEvent} from './components/heatmapHeatScale';

export class HeatmapApi {

    private chart!: Headmap;

    public chartCreated(): boolean {
        return !!this.chart && this.chart instanceof Headmap;
    }

    public create(elementId: string, chartConfig: any & object): Headmap {
        return this.chart = new Headmap(elementId, chartConfig);
    }

    public update(chartConfig: any & object): void {
        if (!this.chartCreated()) {
            console.warn('Update attempt failed: no chart created yet.');

            return;
        }

        this.chart.update(chartConfig);
    }

    public addAnaliseHandler(cb: (event: IAnalyseEvent) => void): void {
        if (this.chartCreated()) {
            this.chart.setHandlerForAnalise(cb);
        }
    }

    public destruct(): void {
        if (!this.chartCreated()) {
            return;
        }

        this.chart.destruct();
        this.chart = null;
    }
}
