import {SmartDomElement} from '../tools/smartDomElement';
import {HeatmapBaseComponent} from './heatmapBaseComponent';
import {ISize} from '../heatmap';
import {round} from '../tools/helper';
import {IAnaliseProperty} from '../heatmapService';

export interface IAnalyseEvent {
    originEvent: any & Event;
    analyseProps: Array<IAnaliseProperty>;
    selectAnalyseProperty: (value: string) => void;
}

export class HeatmapHeatScale extends HeatmapBaseComponent {

    private onScaleExtraHandler!: (bottomValue: number, topValue: number) => void;

    private onAnaliseHandler!: () => void;

    private heatScale: SmartDomElement = new SmartDomElement('.heat-scale');

    private heatScaleTooltip: SmartDomElement = new SmartDomElement('.heat-scale .heatscale-tooltip');

    public draw(svgSize: ISize, leftAxisSize: ISize, bottomAxisSize: ISize): void {
        const {boundaryMargin, chartMargin} = this.baseConfig.axes;

        const bottomAxisFullHeight = bottomAxisSize.height + chartMargin + boundaryMargin;
        const verticalAvailableHeight = svgSize.height - bottomAxisFullHeight;

        const heatScaleFullWidth = this.baseConfig.heatScale.width + chartMargin + boundaryMargin;
        const leftAxisFullWidth = leftAxisSize.width + chartMargin + boundaryMargin;
        const horizontalAxisAvailableWidth = svgSize.width - leftAxisFullWidth - heatScaleFullWidth;
        const translateLeft = leftAxisFullWidth + horizontalAxisAvailableWidth + chartMargin;

        const {gradient, dynamicGradient} = this.config.options.scaleConfig[
            this.service.getSelectedAnalysePropertyValue()
        ];

        if (this.heatScale.empty()) {
            this.heatScale = this.mainSVG
                .append('g')
                .classed('heat-scale', true)
                .style(this.baseConfig.textStyles.heatScale)
            ;
        } else {
            this.heatScale.selectAll('*').forEach((item: SmartDomElement) => {
                item.remove();
            });
        }

        this.mainSVG.select('defs').remove();

        const defs = this.mainSVG.append('defs');
        const linearGradient = defs
            .append('linearGradient')
            .attr('id', 'heatscale-gradient')
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', 0)
            .attr('y2', '100%')
        ;

        [...gradient]
            .reverse()
            .forEach((item: {offset: number, color: number}) => {
                linearGradient
                    .append('stop')
                    .attr('offset', (100 - item.offset) + '%')
                    .attr('stop-color', item.color)
                ;
            })
        ;

        const heatScaleRect = this.heatScale
            .attr('transform', 'translate(' + round(translateLeft) + ', 0)')
            .append('rect')
            .style('fill', 'url(#heatscale-gradient)')
            .attr('x', 0)
            .attr('y', 0)
            .attr('rx', this.baseConfig.heatScale.radius)
            .attr('ry', this.baseConfig.heatScale.radius)
            .attr('width', this.baseConfig.heatScale.radius * 2)
            .attr('height', round(verticalAvailableHeight))
        ;

        if (!dynamicGradient) {
            defs
                .append('clipPath')
                .attr('id', 'scale-clip')
                .append('rect')
                .attr('id', 'scale-clip-rect')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', this.baseConfig.heatScale.radius * 2)
                .attr('height', round(verticalAvailableHeight))
            ;

            heatScaleRect.attr('clip-path', 'url(#scale-clip)');
        }

        this.addHeatLabels(verticalAvailableHeight);
        this.addSliderControls(verticalAvailableHeight);
    }

    public onScaleUpdate(cb: (bottomValue: number, topValue: number) => void): void {
        this.onScaleExtraHandler = cb;
    }

    public onAnalisePropertySelected(cb: () => void): void {
        this.onAnaliseHandler = cb;
    }

    private redrawScaleRect(topY: number, bottomY: number): void {
        const heatScaleRect = new SmartDomElement('.heat-scale rect');
        const selectedProperty = this.service.getSelectedAnalysePropertyValue();

        if (!this.config.options.scaleConfig[selectedProperty].dynamicGradient) {
            new SmartDomElement('#scale-clip-rect')
                .attr('y', topY + this.baseConfig.heatScale.radius)
                .attr('height', bottomY - topY - this.baseConfig.heatScale.radius * 2)
            ;
        } else {
            heatScaleRect
                .style('fill', 'url(#heatscale-gradient)')
                .attr('x', 0)
                .attr('y', topY)
                .attr('height', bottomY - topY)
            ;
        }
    }

    private addHeatLabels(height: number): void {
        const textHeight = parseFloat(this.baseConfig.textStyles.heatScale['font-size']);
        const heatScaleRadius = this.baseConfig.heatScale.radius;
        const scaleConfig = this.config.options.scaleConfig[
            this.service.getSelectedAnalysePropertyValue()
        ];

        const {step, minValue, maxValue} = this.service;

        let textY, labelY, heatLabelText;

        if (this.heatScale.empty()) {
            return;
        }

        const heatScaleLabels = this.heatScale
            .append('g')
            .classed('heat-scale-labels', true)
        ;

        this.attachHeatScaleDropdown(heatScaleLabels);

        // set labels precision from scale config's step value
        const precision = String(step).indexOf('.') > 0 ? String(step).split('.')[1].length : 0;

        for (let currentValue = minValue; currentValue < maxValue + step; currentValue += step) {

            if (currentValue >= maxValue && currentValue < maxValue + step) {
                textY = textHeight + 1;
                labelY = heatScaleRadius + 1;
                heatLabelText = Math.min(currentValue, maxValue).toFixed(precision);
            } else if (currentValue === minValue) {
                textY = round(height - 0.5 * textHeight + 1);
                labelY = round(height - heatScaleRadius);
                heatLabelText = currentValue.toFixed(precision);
            } else {
                const extraValue = ((currentValue - minValue) / (maxValue - minValue)) * (height - heatScaleRadius * 2 - 2);

                textY = round(height - extraValue - textHeight / 2 + 1);
                labelY = round(height - extraValue - heatScaleRadius);
                heatLabelText = currentValue.toFixed(precision);
            }

            heatScaleLabels
                .append('line')
                .classed('heatscale-division-line', true)
                .attr('x1', 20)
                .attr('y1', labelY)
                .attr('x2', 30)
                .attr('y2', labelY)
                .attr('stroke', this.baseConfig.heatScale.delColor)
                .attr('stroke-width', 1)
            ;

            if (scaleConfig.units) {
                heatLabelText += scaleConfig.units;
            }

            heatScaleLabels
                .append('text')
                .classed('heatscale-division-text', true)
                .attr('x', 35)
                .attr('y', textY)
                .text(heatLabelText)
            ;
        }

        heatScaleLabels
            .append('line')
            .classed('heatscale-divisions', true)
            .attr('x1', 20)
            .attr('y1', heatScaleRadius)
            .attr('x2', 20)
            .attr('y2', height - heatScaleRadius)
            .attr('stroke', this.baseConfig.heatScale.delColor)
            .attr('stroke-width', 1)
        ;
    }

    private attachHeatScaleDropdown(heatScaleLabels: SmartDomElement): void {
        const $this = this;
        const stateService = this.service;
        const analyseProps = stateService.analyseProps;

        heatScaleLabels.on('click', function(event: MouseEvent): void {
            event.stopPropagation();

            if ($this.service.analiseHandler && typeof $this.service.analiseHandler === 'function') {
                $this.service.analiseHandler({
                    originEvent: event,
                    analyseProps,
                    selectAnalyseProperty: (value: string) => {
                        stateService.selectAnalyseProp(value);

                        $this.onAnaliseHandler();
                    }
                });
            }
        });

        heatScaleLabels.on('mouseenter', function(this: SmartDomElement): void {
            this.style({
                'fill': $this.config.options.primaryColor || $this.baseConfig.textStyles.common.fill,
                'cursor': 'pointer'
            });
        });

        heatScaleLabels.on('mouseleave', function(this: SmartDomElement): void {
            this.attr('style', '');
        });
    }

    private addSliderControls(height: number): void {
        const $this = this;

        const scaleRadius = this.baseConfig.heatScale.radius;
        const scaleHeight = height - scaleRadius * 2;
        const {minValue, maxValue} = $this.service;

        let bottomYPos = height - scaleRadius;
        let bottomValue = $this.service.minValue;
        let topValue = $this.service.maxValue;
        let topYPos = scaleRadius;

        const minDistance = 10;
        let topSlider, bottomSlider;

        const updateHeatmapOnSlide = (scaleTop: number, scaleBottom: number) => {
            this.redrawScaleRect(
                topYPos - scaleRadius,
                bottomYPos + scaleRadius
            );

            this.onScaleExtraHandler(scaleBottom, scaleTop);
        };

        topSlider = this.heatScale
            .append('g')
            .classed('heatscale-slider', true)
            .attr('transform', 'translate(0,' + topYPos + ')')
            .style('cursor', '-webkit-grab')
            .style('cursor', 'grab')
            .on('dragstart', function(this: SmartDomElement): void {
                this.style('cursor', '-webkit-grabbing')
                    .style('cursor', 'grabbing')
                ;
            })
            .on('dragend', function(this: SmartDomElement): void {
                this.style('cursor', '-webkit-grab')
                    .style('cursor', 'grab')
                ;

                $this.hideScaleTooltip();
            })
            .on('drag', function(this: SmartDomElement, event: MouseEvent): void {
                topYPos = event.y;

                if (topYPos > bottomYPos - minDistance) {
                    topYPos = bottomYPos - minDistance;
                }

                if (topYPos < scaleRadius) {
                    topYPos = scaleRadius;
                }

                topValue = maxValue - (topYPos - scaleRadius) / scaleHeight * (maxValue - minValue);

                updateHeatmapOnSlide(topValue, bottomValue);
                $this.updateScaleTooltip(topValue, round(topYPos));

                this.attr('transform', 'translate(0,' + round(topYPos) + ')');
            })
        ;

        bottomSlider = this.heatScale
            .append('g')
            .classed('heatscale-slider', true)
            .attr('transform', 'translate(0,' + bottomYPos + ')')
            .style('cursor', '-webkit-grab')
            .style('cursor', 'grab')
            .on('dragstart', function(this: SmartDomElement): void {
                this.style('cursor', '-webkit-grabbing')
                    .style('cursor', 'grabbing')
                ;
            })
            .on('dragend', function(this: SmartDomElement): void {
                this.style('cursor', '-webkit-grab')
                    .style('cursor', 'grab')
                ;

                $this.hideScaleTooltip();
            })
            .on('drag', function(event: MouseEvent): void {
                bottomYPos = event.y;

                if (bottomYPos < topYPos + minDistance) {
                    bottomYPos = topYPos + minDistance;
                }

                if (bottomYPos > height - scaleRadius) {
                    bottomYPos = height - scaleRadius;
                }

                bottomValue = maxValue - (bottomYPos - scaleRadius) / scaleHeight * (maxValue - minValue);

                updateHeatmapOnSlide(topValue, bottomValue);
                $this.updateScaleTooltip(bottomValue, round(bottomYPos));

                this.attr('transform', 'translate(0,' + round(bottomYPos) + ')');
            })
            .attr('transform', 'translate(0,' + round(bottomYPos) + ')')
        ;

        this.addSliderContent(topSlider, 1);
        this.addSliderContent(bottomSlider, 0);
        this.addScaleTooltip();
    }

    private addSliderContent(slider: SmartDomElement, offset: number): void {
        slider
            .append('circle')
            .attr('cx', this.baseConfig.heatScale.radius)
            .attr('cy', offset)
            .attr('r', this.baseConfig.heatScale.radius)
            .attr('fill', '#ffffff')
            .attr('stroke', this.baseConfig.heatScale.delColor)
            .attr('stroke-width', 1)
        ;
    }

    private addScaleTooltip(): void {
        const tooltipWidth = this.baseConfig.heatScale.width - 25;
        const tooltipHeight = 22;

        if (this.heatScaleTooltip.empty()) {
            this.heatScaleTooltip = new SmartDomElement('.heat-scale')
                .append('svg')
                .attr('width', tooltipWidth)
                .attr('height', tooltipHeight)
                .attr('y', -tooltipHeight / 2)
                .attr('x', 25)
                .classed('heatscale-tooltip', true)
                .style('opacity', 0)
                .style('fill', '#ffffff')
            ;
        }

        this.heatScaleTooltip
            .append('rect')
            .classed('heatscale-tooltip-rect', true)
            .style('fill', this.baseConfig.textStyles.common.fill)
            .attr('x', 0)
            .attr('y', 0)
            .attr('rx', 4)
            .attr('ry', 4)
            .attr('width', tooltipWidth)
            .attr('height', tooltipHeight)
        ;

        this.heatScaleTooltip
            .append('text')
            .classed('heatscale-tooltip-text', true)
            .style('font-size', '12px')
            .attr('y', '50%')
            .attr('x', '50%')
            .attr('alignment-baseline', 'middle')
            .attr('text-anchor', 'middle')
        ;
    }

    private hideScaleTooltip(): void {
        this.heatScaleTooltip.animation('opacity', '0', 200);
    }

    private updateScaleTooltip(label: string | number, position: number): void {
        const scaleConfig = this.config.options.scaleConfig[
            this.service.getSelectedAnalysePropertyValue()
        ];

        let heatLabelText = round(label, 3);

        if (scaleConfig.units) {
            heatLabelText += scaleConfig.units;
        }

        this.heatScaleTooltip
            .animation('opacity', '1', 200)
            .attr('y', position - 11)
        ;

        this.heatScaleTooltip
            .select('.heatscale-tooltip-text')
            .text(heatLabelText)
        ;
    }
}
