import {SmartDomElement} from '../tools/smartDomElement';
import {HeatmapService} from '../heatmapService';

export abstract class HeatmapBaseComponent {

    protected readonly mainSVG!: SmartDomElement;

    protected readonly config: any & object;

    protected readonly baseConfig: any & object;

    protected service!: HeatmapService;

    constructor(svg: SmartDomElement, config: any & object, baseConfig: any & object, service: HeatmapService) {
        this.mainSVG = svg;
        this.config = config;
        this.baseConfig = baseConfig;
        this.service = service;
    }
}
