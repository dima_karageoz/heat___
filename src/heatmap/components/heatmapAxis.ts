import {SmartDomElement} from '../tools/smartDomElement';
import {ISize} from '../heatmap';
import {HeatmapService} from '../heatmapService';
import {HeatmapBaseComponent} from './heatmapBaseComponent';
import {getElementSizeFactory, ISizeFactory, round} from '../tools/helper';

type TLabel = {
    label: string;
};

interface IAxisData {
    data: Array<string>;
    axis: SmartDomElement;
    size: ISizeFactory;
    classes: string;
    textAnchor: string;
}

export class HeatmapAxis extends HeatmapBaseComponent {

    public readonly leftAxisSize!: ISizeFactory;

    public readonly bottomAxisSize!: ISizeFactory;

    private leftAxis: SmartDomElement = new SmartDomElement('.left-axis');

    private bottomAxis: SmartDomElement = new SmartDomElement('.bottom-axis');

    constructor(svg: SmartDomElement, config: any & object, baseConfig: any & object, service: HeatmapService) {
        super(svg, config, baseConfig, service);

        this.leftAxisSize = getElementSizeFactory(this.leftAxis);
        this.bottomAxisSize = getElementSizeFactory(this.bottomAxis);
    }

    public triggerSvgResize(): void {
        this.leftAxisSize.reset();
        this.bottomAxisSize.reset();
    }

    public setLeftAxisData(): void {
        this.leftAxis = this.setDataToAxis({
            data: this.config.data.axisLeft.data.map((tick: TLabel) => tick.label).reverse(),
            axis: this.leftAxis,
            size: this.leftAxisSize,
            classes: 'left-axis',
            textAnchor: 'end'
        });
    }

    public setBottomAxisData(): void {
        this.bottomAxis = this.setDataToAxis({
            data: this.config.data.axisBottom.data.map((tick: TLabel) => tick.label),
            axis: this.bottomAxis,
            size: this.bottomAxisSize,
            classes: 'bottom-axis',
            textAnchor: 'middle'
        });
    }

    public alignBottomAxis(svgSize: ISize): void {
        const {boundaryMargin, chartMargin} = this.baseConfig.axes;
        const heatScaleFullWidth = this.baseConfig.heatScale.width + chartMargin + boundaryMargin;
        const leftAxisFullWidth = this.leftAxisSize().width + chartMargin + boundaryMargin;
        const horizontalAxisAvailableWidth = svgSize.width - leftAxisFullWidth - heatScaleFullWidth;
        const cellWidth = horizontalAxisAvailableWidth / this.config.data.axisBottom.data.length;

        let maxTextWidth = 0;

        this.bottomAxis
            .selectAll('text')
            .forEach((element: SmartDomElement) => {
                const textRectSize = element.node().getBoundingClientRect();
                const textRectWidth = textRectSize.right - textRectSize.left;

                if (textRectWidth >= maxTextWidth) {
                    maxTextWidth = textRectWidth;
                }
            })
        ;

        this.bottomAxis.attr('transform',
            `translate(${
                round(leftAxisFullWidth)
            },${
                round(svgSize.height - boundaryMargin)
            })`
        );

        if (maxTextWidth >= cellWidth) {
            let rotateAngle = -25;
            let cos = Math.cos(rotateAngle * Math.PI / 180);
            const leftFreeSpace = leftAxisFullWidth + cellWidth / 2;

            if (maxTextWidth >= leftFreeSpace) {
                cos = leftFreeSpace / maxTextWidth;
                rotateAngle = round(-(Math.acos(cos) * 180 / Math.PI));
            }

            const sin = Math.sqrt(1 - Math.pow(cos, 2));
            const tan = sin / cos;
            const yPos = round(-(maxTextWidth < leftFreeSpace ? maxTextWidth : leftFreeSpace) * tan);
            const xPosShift = parseFloat(this.baseConfig.textStyles.axis['font-size']) * sin / 2 - this.baseConfig.cell.borderWidth;

            this.bottomAxis.selectAll('text')
                .forEach((element: SmartDomElement, i: number) => {
                    element
                        .attr(
                            'transform',
                            `rotate(${rotateAngle}, ${round(cellWidth * (i + 0.5) + xPosShift)}, ${yPos})`
                        )
                        .attr('text-anchor', 'end')
                        .attr('x', round(cellWidth * (i + 0.5) + xPosShift))
                        .attr('y', yPos)
                    ;
                })
            ;

        } else {
            this.bottomAxis.selectAll('text')
                .forEach((element: SmartDomElement, i: number) => {
                    element
                        .attr('x', round(cellWidth * (i + 0.5)))
                        .attr('y', 0)
                    ;
                })
            ;
        }
    }

    public alignLeftAxis(svgSize: ISize): void {
        const halfTextSize = parseFloat(this.baseConfig.textStyles.axis['font-size']) / 2;
        const cellBorderWidth = this.baseConfig.cell.borderWidth;
        const {boundaryMargin, chartMargin} = this.baseConfig.axes;

        const leftAxisFullWidth = this.leftAxisSize().width + chartMargin + boundaryMargin;
        const bottomAxisFullHeight = this.bottomAxisSize().height + chartMargin + boundaryMargin;
        const cellHeight = (svgSize.height - bottomAxisFullHeight) / this.config.data.axisLeft.data.length;

        this.leftAxis
            .attr('transform', `translate(${round(leftAxisFullWidth - chartMargin)}, 0)`)
            .selectAll('text')
            .forEach((elem: SmartDomElement, i: number) => {
                elem.attr('y', round(cellHeight * (i + 0.5) + halfTextSize - cellBorderWidth));
            })
        ;
    }

    private setDataToAxis(options: IAxisData): SmartDomElement {
        const {axis, size, classes, data, textAnchor} = options;

        let nextAxis = axis;

        if (nextAxis.empty()) {
            nextAxis = this.mainSVG
                .append('g')
                .classed(classes, true)
                .style(this.baseConfig.textStyles.axis)
            ;

            size.updateElement(nextAxis);
        } else {
            nextAxis.text('');
        }

        data.forEach((label: string) => {
            nextAxis.append('text')
                .classed(classes, true)
                .attr('text-anchor', textAnchor)
                .text(label)
            ;
        });

        return nextAxis;
    }
}
