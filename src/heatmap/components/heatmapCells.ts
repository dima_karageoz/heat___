import {SmartDomElement} from '../tools/smartDomElement';
import {ISize, TEventMap} from '../heatmap';
import {HeatmapBaseComponent} from './heatmapBaseComponent';
import {round} from '../tools/helper';


type TLabel = {
    label: string;
};

interface ICellOptions {
    config: any & object;
    width: number;
    height: number;
    xIndex: number;
    yIndex: number;
    textParams: any & object;
    eventHandlers: TEventMap;
}

interface ICellText {
    value: number;
    xPos: number;
    yPos: number;
    alignToEnd: boolean;
    color: string;
}

export class HeatmapCells extends HeatmapBaseComponent {

    private cellsContainer: SmartDomElement = new SmartDomElement('.cells');

    private cells: Array<SmartDomElement> = [];

    private cellsRect: Array<SmartDomElement> = [];

    private readonly cellHandlers: TEventMap = {
        'mouseenter': function(this: SmartDomElement): void {
            this
                .select('.cell-info')
                .animation('opacity', '1', 200)
            ;
        },
        'mouseleave': function(this: SmartDomElement): void {
            this
                .select('.cell-info')
                .animation('opacity', '0', 200)
            ;
        }
    };

    public draw(svgSize: ISize, leftAxis: ISize, rightAxis: ISize): void {
        const {boundaryMargin, chartMargin} = this.baseConfig.axes;
        const margin = boundaryMargin + chartMargin;
        const cellsConfig = this.config.data.cells;

        const heatScaleFullWidth = this.baseConfig.heatScale.width + margin;
        const leftAxisFullWidth = leftAxis.width + margin;
        const bottomAxisFullHeight = rightAxis.height + margin;

        const leftAxisLabels = this.config.data.axisLeft.data.map((tick: TLabel) => tick.label);
        const bottomAxisLabels = this.config.data.axisBottom.data.map((tick: TLabel) => tick.label);

        const cellWidth = (svgSize.width - leftAxisFullWidth - heatScaleFullWidth) / bottomAxisLabels.length;
        const cellHeight = (svgSize.height - bottomAxisFullHeight) / leftAxisLabels.length;
        const cellTextParams = this.getCellTextParams(cellWidth, cellHeight);

        if (this.cellsContainer.empty()) {
            this.cellsContainer = this.mainSVG
                .append('g')
                .classed('cells', true)
                .style('cursor', 'pointer')
            ;
        } else {
            this.removeAllCells();
        }

        this.cellsContainer
            .style('font-size', cellTextParams.size + 'px')
            .attr('transform', 'translate(' + round(leftAxisFullWidth) + ', 0)')
        ;

        cellsConfig.forEach((item: any & object) => {
            const indexInLeftAxis = leftAxisLabels.indexOf(item.y);

            this.addCell({
                config: item[this.service.getSelectedAnalysePropertyValue()],
                width: cellWidth,
                height: cellHeight,
                xIndex: bottomAxisLabels.indexOf(item.x),
                yIndex: leftAxisLabels.length - 1 - indexInLeftAxis,
                textParams: cellTextParams,
                eventHandlers: item.eventHandlers
            });
        });
    }

    public redraw(topSlideValue: number, bottomSlideValue: number): void {
        let maxValueInRange = bottomSlideValue;
        let minValueInRange = topSlideValue;

        this.cells
            .forEach((cell: SmartDomElement, index: number) => {
                const rect = this.cellsRect[index];
                const avgValue = parseFloat(cell.getDatumData());

                let isInHeatRange = avgValue <= topSlideValue && avgValue >= bottomSlideValue;

                if (avgValue < minValueInRange && isInHeatRange) {
                    minValueInRange = avgValue;
                }

                if (avgValue > maxValueInRange && isInHeatRange) {
                    maxValueInRange = avgValue;
                }

                isInHeatRange = avgValue <= maxValueInRange && avgValue >= minValueInRange;

                const cellColor = isInHeatRange ? this.service.colorScale(avgValue) : this.baseConfig.cell.initialColor;

                rect.animation('fill', cellColor, 200);

                rect.style('pointer-events', isInHeatRange ? 'all' : 'none');
            })
        ;
    }

    private removeAllCells(): void {
        this.cells.forEach((cell: SmartDomElement) => cell.remove());

        this.cells = [];
        this.cellsRect = [];
    }

    private addCell(options: ICellOptions): void {
        const cellStyle = this.baseConfig.cell;
        const cellTextColor = this.baseConfig.textStyles.common.fill;
        const cellBorderWidth = this.baseConfig.cell.borderWidth;

        if (options.xIndex === -1 || options.yIndex === -1) {
            return;
        }

        if (options.width < 0) {
            options.width = 0;
        }

        if (options.height < 0) {
            options.height = 0;
        }

        const xTranslate = round(options.xIndex * options.width);
        const yTranslate = round(options.yIndex * options.height);

        const cell = this.cellsContainer
            .append('g')
            .datum(options.config ? options.config.conjunction : null)
            .classed('cell', true)
            .attr('transform', `translate(${xTranslate},${yTranslate})`)
        ;

        const cellColor = options.config ? this.service.colorScale(options.config.conjunction) : cellStyle.initialColor;

        const rect = cell
            .append('rect')
            .classed('cell-rect', true)
            .attr('rx', 4)
            .attr('ry', 4)
            .attr('width', round(options.width))
            .attr('height', round(options.height))
            .attr('stroke', 'rgb(255,255,255)')
            .attr('stroke-width', cellBorderWidth)
            .style('fill', cellStyle.initialColor)
            .animation('fill', cellColor, 750)
        ;

        if (options.config) {
            const cellInfo = cell
                .append('g')
                .classed('cell-info', true)
                .style('opacity', 0)
            ;

            this.insertCellLine(cellInfo, options.width, options.height);

            this.insertText(cellInfo, {
                value: options.config.xValue,
                xPos: options.width - options.textParams.xMargin - cellBorderWidth,
                yPos: options.height - options.textParams.yMargin - cellBorderWidth,
                alignToEnd: true,
                color: cellTextColor
            });

            this.insertText(cellInfo, {
                value: options.config.yValue,
                xPos: options.textParams.xMargin + cellBorderWidth,
                yPos: options.textParams.size,
                alignToEnd: false,
                color: cellTextColor
            });
        } else {
            cell.style('pointer-events', 'none');
        }

        if (!options.eventHandlers) {
            cell.style('cursor', 'default');
        }

        this.attachHandlers(cell, options.eventHandlers);

        this.cells.push(cell);
        this.cellsRect.push(rect);
    }

    private insertCellLine(cellInfo: SmartDomElement, cellWidth: number, cellHeight: number): void {
        const cellBorderWidth = this.baseConfig.cell.borderWidth;

        cellInfo
            .append('line')
            .classed('cell-line', true)
            .attr('x1', 1)
            .attr('y1', round(cellHeight - cellBorderWidth))
            .attr('x2', round(cellWidth - cellBorderWidth))
            .attr('y2', 1)
            .attr('stroke', 'rgb(255,255,255)')
            .attr('stroke-width', cellBorderWidth)
        ;
    }

    private insertText(cell: SmartDomElement, textConfig: ICellText): void {
        const scaleConfig = this.config.options.scaleConfig[
            this.service.getSelectedAnalysePropertyValue()
        ];

        let cellTextLabel = Math.round(textConfig.value * 1000) / 1000;

        if (scaleConfig.units) {
            cellTextLabel += scaleConfig.units;
        }

        const cellText = cell
            .append('text')
            .classed('cell-text', true)
            .style('fill', textConfig.color)
            .attr('x', round(textConfig.xPos))
            .attr('y', round(textConfig.yPos))
            .text(cellTextLabel)
        ;

        if (textConfig.alignToEnd) {
            cellText.attr('text-anchor', 'end');
        } else {
            cellText.attr('text-anchor', 'start');
        }
    }

    private attachHandlers(cell: SmartDomElement, eventHandlers: TEventMap): void {
        if (cell.empty()) {
            return;
        }

        for (const defaultEventName in this.cellHandlers) {
            if (!this.cellHandlers.hasOwnProperty(defaultEventName)) continue;

            cell.on(defaultEventName, this.cellHandlers[defaultEventName]);
        }

        for (const eventName in eventHandlers) {
            if (!eventHandlers.hasOwnProperty(eventName)) continue;

            cell.on(eventName, eventHandlers[eventName]);
        }
    }

    private getCellTextParams(cellWidth: number, cellHeight: number): {
        size: number,
        xMargin: number,
        yMargin: number
    } {
        const defaultSize = parseFloat(this.baseConfig.textStyles.common['font-size']);
        const cellBorderWidth = this.baseConfig.cell.borderWidth;
        const scaleConfig = this.config.options.scaleConfig[
            this.service.getSelectedAnalysePropertyValue()
        ];

        let defs = this.mainSVG.select('defs');

        let xMargin = 5;
        let yMargin = 5;

        if (!cellWidth || !cellHeight) {
            return {
                size: defaultSize,
                xMargin: xMargin,
                yMargin: yMargin
            };
        }

        if (cellWidth / 20 < xMargin) {
            xMargin = Math.floor(cellWidth / 20);
        }

        if (cellHeight / 20 < yMargin) {
            yMargin = Math.floor(cellHeight / 20);
        }

        if (defs.empty()) {
            defs = this.mainSVG.append('defs');
        }

        let defText = round(scaleConfig.maxValue || '100', 3);

        if (scaleConfig.units) {
            defText += scaleConfig.units;
        }

        let textEl = defs.select('text');

        if (!textEl.empty()) {
            textEl.text(defText);
        } else {
            textEl = defs
                .append('text')
                .text(defText)
            ;
        }

        const textElRect = textEl.node().getBoundingClientRect();

        const textSize = {
            width: textElRect.right - textElRect.left,
            height: textElRect.bottom - textElRect.top
        };

        const tanA = textSize.height / textSize.width;
        const tanB = cellHeight / cellWidth;

        const textContainer = {
            width: cellWidth * tanB / (tanA + tanB) - cellBorderWidth - 2 * xMargin,
            height: cellWidth * tanA * tanB / (tanA + tanB) - cellBorderWidth - 2 * yMargin
        };

        const sizeScale = textSize.width / textContainer.width;

        return {
            size: sizeScale < 1 ? defaultSize : Math.floor(defaultSize / sizeScale),
            xMargin: xMargin,
            yMargin: yMargin
        };
    }
}
