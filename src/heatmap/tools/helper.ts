import {SmartDomElement} from './smartDomElement';
import {ISize} from '../heatmap';

export interface ISizeFactory {
    (): ISize;

    reset: () => void;
    updateElement: (elem: SmartDomElement) => void;
}

export const round = (value: number | string, precision?: number): number => {
    if (precision === undefined) {
        precision = 2;
    }

    if (typeof value === 'string') {
        return parseFloat((+value).toFixed(precision));
    }

    if (typeof value === 'number') {
        return parseFloat(value.toFixed(precision));
    }

    return value;
};

export const getElementSizeFactory = (element: SmartDomElement): ISizeFactory => {
    let cache: any & object;
    let sizeElement = element;

    const findSizeFunction = () => {
        if (cache) {
            return cache;
        }

        if (sizeElement.empty()) {
            return { width: 0, height: 0 };
        }

        const boundingRect = sizeElement.node().getBoundingClientRect();

        cache = {
            width: boundingRect.right - boundingRect.left,
            height: boundingRect.bottom - boundingRect.top
        };

        return cache;
    };

    findSizeFunction.reset = () => {
        cache = null;
    };

    findSizeFunction.updateElement = (elem: SmartDomElement) => {
        cache = null;
        sizeElement = elem;
    };

    return findSizeFunction;
};

export const createColorRange = (range: Array<number>, colors: Array<string>): (value: number) => string => {
    if (!Array.isArray(range)
        || !Array.isArray(colors)
        || range.length !== colors.length
    ) {
        throw new Error('Arguments range and colors have to be array');
    }

    const cache: {[key: number]: string} = {};

    return (value: number) => {
        if (cache[value]) {
            return cache[value];
        }

        let startRangeIndex = 0;

        for (let i = 1; i < range.length; i++) {
            if (range[i] >= value) {
                break;
            }

            startRangeIndex++;
        }

        const startColor = colors[startRangeIndex];
        const endColor = colors[startRangeIndex + 1];

        const percentage = (value - range[startRangeIndex]) / (range[startRangeIndex + 1] - range[startRangeIndex]) * 100;
        const resultColor = ColorsGenerator.getColorByPercentage(startColor, endColor, percentage);

        cache[value] = resultColor;

        return resultColor;
    };
};

/* tslint:disable:no-bitwise */
class ColorsGenerator {

    private static hexToRgbCache: {[key: string]: Array<number>} = {};

    private static hexToRgb(color: string): Array<number> {
        const colorString = color[0] === '#' ? color.slice(1) : color;

        if (this.hexToRgbCache[color]) {
            return this.hexToRgbCache[color];
        }

        this.hexToRgbCache[color] = [0, 2, 4].map((index: number) =>
            parseInt(
                colorString.length === 6
                    ? colorString.slice(index, index + 2)
                    : colorString[index / 2] + colorString[index / 2],
                16
            )
        );

        return this.hexToRgbCache[color];
    }

    private static rgbToHex(rgb: Array<number>): string {
        const toHexSystem = (numb: number) => {
            let hex = Number(numb).toString(16);

            if (hex.length < 2) {
                hex = '0' + hex;
            }

            return hex;
        };

        return '#' + rgb.map((color: number) => toHexSystem(color)).join('');
    }

    public static getColorByPercentage(basicColor: string, finalColor: string, percentage: number): string {
        const startRGB: Array<number> = this.hexToRgb(basicColor);
        const endRGB: Array<number> = this.hexToRgb(finalColor);

        const rgb = startRGB.map((item: number, index: number) => {
            const redDiff = percentage / 100 * Math.abs(endRGB[index] - startRGB[index]);

            const color = startRGB[index] < endRGB[index]
                ? startRGB[index] + redDiff
                : startRGB[index] - redDiff
            ;

            return Math.round(color);
        });

        return this.rgbToHex(rgb);
    }
}

