export const heatmapBaseConfig = {
    textStyles: {
        common: {
            'font-weight': 'normal',
            'font-style': 'normal',
            'font-size': '16px',
            'fill': '#494a4c'
        },
        axis: {
            'font-size': '12px'
        },
        heatScale: {
            'font-size': '14px'
        }
    },
    axes: {
        chartMargin: 10,
        boundaryMargin: 5
    },
    cell: {
        initialColor: '#fffbf4',
        borderWidth: 1
    },
    heatScale: {
        width: 80,
        radius: 10,
        delColor: '#494a4c'
    },
    dropdown: {
        width: 130
    }
};
