export class SmartDomElement {

    public on: (eventName: string, callback: (event: any & Event) => void) => SmartDomElement = this.eventHandlerFactory();

    public animation: (propertyName: string, value: string, animationTime: number) => SmartDomElement = this.animationFactory();

    private readonly currentElement!: HTMLElement | SVGElement;

    constructor(elem: HTMLElement | SVGElement | string ) {
        if (typeof elem === 'string') {
            this.currentElement = document.querySelector(elem);
        } else {
            this.currentElement = elem;
        }
    }

    public select(query: string): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        return new SmartDomElement(this.currentElement.querySelector<HTMLElement>(query));
    }

    public selectAll(query: string): Array<SmartDomElement> {
        if (this.empty()) {
            return [];
        }

        return [].slice
            .call(this.currentElement.querySelectorAll(query))
            .map((item: HTMLElement) => new SmartDomElement(item))
        ;
    }

    public getAttrValue(name: string): string | undefined {
        if (this.empty()) {
            return undefined;
        }

        return this.currentElement.getAttribute(name);
    }

    public attr(name: string, value: string | number = ''): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        if (name && name.length) {
            this.currentElement.setAttribute(name, '' + value);
        } else {
            console.error('Incorrect attribute name');
        }

        return this;
    }

    public append(tagName: string, options: { svgElement: boolean } = { svgElement: true }): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        const child = options && options.svgElement
            ? document.createElementNS('http://www.w3.org/2000/svg', tagName)
            : document.createElement(tagName)
        ;

        this.currentElement.appendChild(child);

        return new SmartDomElement(child);
    }

    public style(style: string| {[key: string]: string}, styleValue?: string | number): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        if (typeof style === 'string') {
            (<any>this.currentElement.style)[this.parseStringToCameCase(style)] = styleValue;
        } else {
            for (const key in style) {
                if (style.hasOwnProperty(key)) {
                    (<any>this.currentElement.style)[this.parseStringToCameCase(key)] = style[key];
                }
            }
        }

        return this;
    }

    public datum(data: any): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        this.currentElement.setAttribute('data-datum', data);

        return this;
    }

    public getDatumData(): any {
        if (this.empty()) {
            return this;
        }

        const datum = this.currentElement.getAttribute('data-datum');

        return datum;
    }

    public node(): HTMLElement | SVGElement {
        return this.currentElement;
    }

    public empty(): boolean {
        return !this.currentElement
            || !(this.currentElement instanceof HTMLElement || this.currentElement instanceof SVGElement)
        ;
    }

    public text(innerText: string | number): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        this.currentElement.textContent = '' + innerText;

        return this;
    }

    public html(innerHTML: string): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        this.currentElement.innerHTML = innerHTML;

        return this;
    }

    public classed(className: string, manipulation?: boolean | string): SmartDomElement {
        if (this.empty()) {
            return this;
        }

        const classListManipulation = (classes: string, action: boolean) => {
            classes.split(' ').forEach((item: string) => {
                action
                    ? this.currentElement.classList.add(item)
                    : this.currentElement.classList.remove(item)
                ;
            });
        };

        switch (true) {
            case manipulation === true:
                classListManipulation(className, true);

                return this;
            case manipulation === false:
                classListManipulation(className, false);

                return this;
            case typeof manipulation === 'string':
                classListManipulation(<string>className, false);
                classListManipulation(<string>manipulation, true);

                return this;
            default:
                return this;
        }
    }

    public remove(): void {
        if (!this.empty() && this.currentElement.parentNode) {
            this.currentElement.parentNode.removeChild(
                this.currentElement
            );
        }
    }

    public clearContent(): void {
        if (!this.empty()) {
            this.currentElement.innerHTML = '';
        }
    }

    public animationFactory(): (propertyName: string, value: string, animationTime: number) => SmartDomElement {
        let lastApplyValue = {
            propertyName: '',
            value: ''
        };

        let animationTimer: number;

        return (propertyName: string, value: string, animationTime: number) => {
            if (lastApplyValue.propertyName !== propertyName
                || lastApplyValue.value !== value
            ) {
                clearTimeout(animationTimer);

                this.currentElement.style.transition = `${propertyName} ${animationTime}ms linear`;

                lastApplyValue = {propertyName, value};

                const style = <any>this.currentElement.style;
                const propertyOfStyle = this.parseStringToCameCase(propertyName);

                style[propertyOfStyle] = value;

                animationTimer = setTimeout(() => {
                    style.transition = '';
                }, animationTime);
            }

            return this;
        };
    }

    public hasClass(className: string): boolean {
        return this.currentElement.classList.contains(className);
    }

    private parseStringToCameCase(src: string): string {
        return src.replace(/(-)[a-zA-Z]/ig, (char: string) => char[1].toUpperCase());
    }

    private eventHandlerFactory(): (eventName: string, callback: (event: any & Event) => void) => SmartDomElement {
        let mouseClickTimeout: number;
        let dragendCallback: (event: any & Event) => void;
        let dragCallback: (event: any & Event) => void;

        const dragStartHandler = (callback: (event: any & Event) => void) => {
            const handler = (event: MouseEvent) => {
                mouseClickTimeout = window.setTimeout(() => {
                    callback.bind(this)(event);
                    window.getSelection().removeAllRanges();

                    const completeDragendCallback = (dragEvent: MouseEvent) => {
                        dragendCallback.bind(this)(dragEvent);
                        document.removeEventListener('mouseup', completeDragendCallback);
                        document.removeEventListener('mousemove', draggingHandler);
                    };

                    const draggingHandler = dragCallback.bind(this);

                    document.addEventListener('mousemove', draggingHandler);
                    document.addEventListener('mouseup', completeDragendCallback);
                }, 170);

                this.currentElement.removeEventListener('click', handler);

                document.addEventListener('mouseup', killStartHandler);
            };

            return handler;
        };

        const killStartHandler = () => {
            clearTimeout(mouseClickTimeout);
            document.removeEventListener('mouseup', killStartHandler);
        };

        return (eventName: string, callback: (event: any & Event) => void) => {
            if (this.empty()) {
                return this;
            }

            switch (eventName) {
                case 'dragstart':
                    const handler = dragStartHandler(callback);

                    this.currentElement.addEventListener('mousedown', handler);

                    break;
                case 'dragend':
                    dragendCallback = callback;

                    break;
                case 'drag':
                    dragCallback = callback;

                    break;
                default:
                    this.currentElement.addEventListener(eventName, callback.bind(this));
            }

            return this;
        };
    }
}
