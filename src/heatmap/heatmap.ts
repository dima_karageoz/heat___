import {SmartDomElement} from './tools/smartDomElement';
import {HeatmapService} from './heatmapService';
import {HeatmapAxis} from './components/heatmapAxis';
import {HeatmapCells} from './components/heatmapCells';
import {HeatmapHeatScale, IAnalyseEvent} from './components/heatmapHeatScale';
import {getElementSizeFactory, ISizeFactory} from './tools/helper';
import {heatmapBaseConfig} from './tools/contans';

export interface ISize {
    height: number;
    width: number;
}

export type TEventMap = {
    [key: string]: (event: Event) => void;
};

export class Headmap {

    private readonly elementId!: string;

    private readonly baseConfig: any & object = heatmapBaseConfig;

    private readonly windowHandlers: TEventMap = {
        'resize': this.resizeChart.bind(this)
    };

    private config!: any & object;

    private rootSvg!: SmartDomElement;

    private axis!: HeatmapAxis;

    private cells!: HeatmapCells;

    private heatScale!: HeatmapHeatScale;

    private svgSize!: ISizeFactory;

    private service: HeatmapService = new HeatmapService();

    constructor(
        elementId: string,
        chartConfig: any & object
    ) {
        this.config = chartConfig;
        this.elementId = elementId;

        this.service.analyseProps = chartConfig.options.analyseProps;

        this.create();
    }

    public update(chartConfig: any): void {
        this.config = chartConfig;
        this.service.analyseProps = chartConfig.options.analyseProps;

        if (!this.checkConfig(this.config)) {
            return;
        }

        this.drawChart();
    }

    public destruct(): void {
        this.detachWindowHandlers();

        this.rootSvg.remove();
    }

    public setHandlerForAnalise(cb: (event: IAnalyseEvent) => void): void {
        this.service.analiseHandler = cb;
    }

    private create(): void {
        if (!this.elementId || typeof this.elementId !== 'string') {
            throw new Error('Invalid element id specified!');
        }

        if (!this.checkConfig(this.config)) {
            return;
        }

        this.drawChart();
        this.attachWindowHandlers();
    }

    private checkConfig(chartConfig: any & object): boolean {
        const errorMessage = this.service.checkConfig(chartConfig);

        if (errorMessage) {
            this.setError(errorMessage);

            return false;
        }

        return true;
    }

    private drawChart(): void {
        if (!this.rootSvg || this.rootSvg.empty()) {
            this.rootSvg = new SmartDomElement('#' + this.elementId)
                .append('svg')
                .attr('width', '100%')
                .attr('height', '100%')
                .style(this.baseConfig.textStyles.common)
            ;

            this.svgSize = getElementSizeFactory(this.rootSvg);

            this.axis = new HeatmapAxis(this.rootSvg, this.config, this.baseConfig, this.service);
            this.cells = new HeatmapCells(this.rootSvg, this.config, this.baseConfig, this.service);
            this.heatScale = new HeatmapHeatScale(this.rootSvg, this.config, this.baseConfig, this.service);

            this.heatScale.onScaleUpdate((bottomValue: number, topValue: number) => {
                if (this.config.options.scaleConfig[this.service.getSelectedAnalysePropertyValue()].dynamicGradient) {
                    this.service.setColorScale(bottomValue, topValue);
                }

                this.cells.redraw(topValue, bottomValue);
            });

            this.heatScale.onAnalisePropertySelected(() => {
                this.drawChart();
            });
        }

        this.service.resetState(this.config);

        // order is important for calculating available space for elements
        this.axis.setLeftAxisData();
        this.axis.setBottomAxisData();

        this.axis.alignBottomAxis(this.svgSize());
        this.axis.alignLeftAxis(this.svgSize());

        this.heatScale.draw(this.svgSize(), this.axis.leftAxisSize(), this.axis.bottomAxisSize());
        this.cells.draw(this.svgSize(), this.axis.leftAxisSize(), this.axis.bottomAxisSize());
    }

    private resizeChart(): void {
        if (this.service.actionsTimeout) {
            return;
        }

        this.service.actionsTimeout = window.setTimeout(() => {
            this.svgSize.reset();

            if (this.axis) {
                this.axis.triggerSvgResize();
            }

            this.axis.alignBottomAxis(this.svgSize());
            this.axis.alignLeftAxis(this.svgSize());

            this.heatScale.draw(this.svgSize(), this.axis.leftAxisSize(), this.axis.bottomAxisSize());
            this.cells.draw(this.svgSize(), this.axis.leftAxisSize(), this.axis.bottomAxisSize());
            this.service.actionsTimeout = null;
        }, 30);
    }

    private attachWindowHandlers(): void {
        for (const eventName in this.windowHandlers) {
            if (this.windowHandlers.hasOwnProperty(eventName)) {
                window.addEventListener(eventName, this.windowHandlers[eventName]);
            }
        }
    }

    private detachWindowHandlers(): void {
        for (const eventName in this.windowHandlers) {
            if (this.windowHandlers.hasOwnProperty(eventName)) {
                window.removeEventListener(eventName, this.windowHandlers[eventName]);
            }
        }
    }

    private setError(errorMsg: string): void {
        const noDataElement = new SmartDomElement('.info-data-doesnot-exist');
        const errorMessage = '<div class="alignment">\
                    <div class="alignment-cell text-md">\
                        <p class="text-center text-lg margin-bottom">\
                            <i class="fa fa-3x fa-lock"></i>\
                        </p>\
                        <span>' + errorMsg + '</span>\
                    </div>\
                </div>'
        ;

        if (!noDataElement.empty()) {
            noDataElement.clearContent();

            noDataElement.html(errorMessage);
        } else {
            new SmartDomElement('#' + this.elementId)
                .append('div', { svgElement: false})
                .classed('info-data-doesnot-exist full-screen', true)
                .html(errorMessage)
            ;
        }
    }
}
