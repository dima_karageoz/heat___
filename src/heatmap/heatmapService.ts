import {createColorRange} from './tools/helper';
import {IAnalyseEvent} from './components/heatmapHeatScale';

export interface IAnaliseProperty {
    active: boolean;
    label: string;
    value: string;
}

export class HeatmapService {

    public maxValue!: number;

    public minValue!: number;

    public step!: number;

    public actionsTimeout!: number | null;

    public colorScale!: (value: number) => string;

    public analiseHandler!: (event: IAnalyseEvent) => void;

    public analyseProps!: Array<IAnaliseProperty>;

    private chartConfig!: any;

    public setColorScale(minValue: number, maxValue: number): void {
        const scalePropertyConfig = this.chartConfig.options.scaleConfig[
            this.getSelectedAnalysePropertyValue()
        ];

        const domainArr: Array<number> = [];
        const colorArr: Array<string> = [];

        for (const item of scalePropertyConfig.gradient) {
            domainArr.push(
                minValue + (maxValue - minValue) / 100 * item.offset
            );

            colorArr.push(item.color);
        }

        this.colorScale = createColorRange(domainArr, colorArr);
    }

    public resetState(chartConfig: any): void {
        if (
            !chartConfig
            || !chartConfig.options
            || !chartConfig.options.scaleConfig
            || !chartConfig.options.analyseProps
        ) {
            console.error('Chart scale config is missing or incorrect!');

            return;
        }

        this.chartConfig = chartConfig;
        this.analyseProps = chartConfig.options.analyseProps;

        const scalePropertyConfig = chartConfig.options.scaleConfig[
            this.getSelectedAnalysePropertyValue()
        ];

        if (scalePropertyConfig.divisions) {
            this.minValue = scalePropertyConfig.divisions.min;
            this.maxValue = scalePropertyConfig.divisions.max;
            this.step = scalePropertyConfig.divisions.step;
        } else {
            this.calculateScaleDivisions();
        }

        this.setColorScale(this.minValue, this.maxValue);
    }

    public getSelectedAnalysePropertyValue(): string {
        if (!this.analyseProps) {
            return;
        }

        for (const item of this.analyseProps) {
            if (item.active) {
                return item.value;
            }
        }
    }

    public selectAnalyseProp(propValue: string | undefined): void {
        for (const item of this.analyseProps) {
            item.active = item.value === propValue;
        }
    }

    public checkConfig(chartConfig: any & object): string | null {

        if (chartConfig && chartConfig.data && chartConfig.data.chartMessage) {
            return chartConfig.data.chartMessage;
        } else if (
            !chartConfig
            || !chartConfig.data
            || !chartConfig.data.axisLeft
            || !chartConfig.data.axisBottom
            || !chartConfig.data.cells
            || !chartConfig.data.cells.length
        ) {
            return 'There is no data available to create this chart.<br>Please contact your system administrator.';
        } else if (
            !chartConfig
            || !chartConfig.options
            || !chartConfig.options.analyseProps
            || !chartConfig.options.scaleConfig
        ) {
            return 'Wrong chart options provided!';
        }

        this.chartConfig = chartConfig;

        return null;
    }

    private calculateScaleDivisions(): void {
        if (!this.chartConfig) {
            return;
        }

        const cells = this.chartConfig.data.cells;
        const analysedProperty = this.getSelectedAnalysePropertyValue();
        let minCellValue = cells[0][analysedProperty].conjunction;
        let maxCellValue = cells[0][analysedProperty].conjunction;

        function findSuitableStep(steps: Array<number>): number {
            const step = (maxCellValue - minCellValue) / 10;

            if (step > steps[3]) {
                steps = steps.map((item: number) => item * 10);

                return findSuitableStep(steps);
            } else if (step < steps[0]) {
                steps = steps.map((item: number) => item / 10);

                return findSuitableStep(steps);
            } else {
                let diff = step - steps[0];
                let closestStep = steps[0];

                for (const stepItem of steps) {
                    if (Math.abs(stepItem - stepItem) <= diff) {
                        diff = Math.abs(stepItem - stepItem);
                        closestStep = stepItem;
                    }
                }

                return closestStep;
            }
        }

        // find min and max among the cell values

        for (const cell of cells) {
            if (!cell[analysedProperty]) {
                continue;
            }

            if (cell[analysedProperty].conjunction > maxCellValue) {
                maxCellValue = cell[analysedProperty].conjunction;
            }

            if (cell[analysedProperty].conjunction < minCellValue) {
                minCellValue = cell[analysedProperty].conjunction;
            }
        }

        // calculate step for the scale divisions
        if (minCellValue === maxCellValue) {
            minCellValue--;
            maxCellValue++;
        }

        this.step = findSuitableStep([1, 2, 5, 10]);
        this.maxValue = Math.ceil(maxCellValue / this.step) * this.step;
        this.minValue = Math.floor(minCellValue / this.step) * this.step;
    }
}
