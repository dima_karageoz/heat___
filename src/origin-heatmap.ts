/* tslint:disable */

declare var d3: any;

var _chartInstance: Heatmap | null = null;

class Heatmap {

    private _config!: any;

    private _elementId!: string;

    private _service: StateService = new StateService();

    private _baseConfigs!: any;

    private _defaultCellHandlers!: any;

    private _windowHandlers!: any;

    constructor(elementId: string, chartConfig: any) {
        this._config = chartConfig;
        this._elementId = elementId;

        this._service.setAnalyseProps(chartConfig.options.analyseProps);

        this._baseConfigs = {
            textStyles: {
                common: {
                    'font-weight': 'normal',
                    'font-style': 'normal',
                    'font-size': '16px',
                    'fill': '#494a4c'
                },
                axis: {
                    'font-size': '12px'
                },
                heatScale: {
                    'font-size': '14px'
                }
            },
            axes: {
                chartMargin: 10,
                boundaryMargin: 5
            },
            cell: {
                initialColor: '#fffbf4',
                borderWidth: 1
            },
            heatScale: {
                width: 80,
                radius: 10,
                delColor: '#494a4c'
            },
            dropdown: {
                width: 130
            }
        };

        this._defaultCellHandlers = {
            'mouseenter': function () {
                d3.select(this).select('.cell-info')
                    .transition()
                    .duration(200)
                    .style('opacity', 1)
                ;
            },
            'mouseleave': function () {
                d3.select(this).select('.cell-info')
                    .transition()
                    .duration(200)
                    .style('opacity', 0)
                ;
            }
        };

        this._windowHandlers = {
            'resize': this._resizeChart.bind(this),
            'click': this._closeAnalyseDropdown.bind(this)
        };

        this.create();
    }

    public create() {
        if (!this._elementId || typeof this._elementId !== 'string') {
            throw 'Invalid element id specified!';
        }

        if (!this._checkConfig(this._config)) {
            return;
        }

        this._drawChart();
        this._attachWindowHandlers();
    }

    public update(chartConfig: any) {
        this._config = chartConfig;
        this._service.setAnalyseProps(chartConfig.options.analyseProps);

        if (!this._checkConfig(this._config)) {
            return;
        }

        this._drawChart();
    }

    public destruct(): any {
        this._detachWindowHandlers();

        if (!this._getSVG().empty()) {
            this._getSVG().remove();
        }
    }

    public getSelectedAnalyseProperty() {
        var selectedProperty = this._service.getSelectedAnalyseProperty();

        if (selectedProperty) {
            return selectedProperty.value;
        }
    }

    // private

    /**
     * check whether chart config contains required options
     * show no data message in case if some option is absent
     */
    public _checkConfig(chartConfig: any) {
        var noDataElement = d3.select('.info-data-doesnot-exist');

        if (!noDataElement.empty()) {
            noDataElement.remove();
        }

        if (chartConfig && chartConfig.data && chartConfig.data.chartMessage) {
            this._setError(chartConfig.data.chartMessage);

            return false;
        } else if (
            !chartConfig
            || !chartConfig.data
            || !chartConfig.data.axisLeft
            || !chartConfig.data.axisBottom
            || !chartConfig.data.cells
            || !chartConfig.data.cells.length
        ) {
            this._setError(
                'There is no data available to create this chart.<br>Please contact your system administrator.'
            );

            return false;
        } else if (
            !chartConfig
            || !chartConfig.options
            || !chartConfig.options.analyseProps
            || !chartConfig.options.scaleConfig
        ) {
            this._setError('Wrong chart options provided!');

            return false;
        }

        return true;
    }

    //=========================== DRAW CHART =============================

    private _drawChart() {
        if (this._getSVG().empty()) {
            d3.select('#' + this._elementId)
                .append('svg')
                .style('width', '100%')
                .style('height', '100%')
                .style(this._baseConfigs.textStyles.common)
            ;
        }

        this._service.resetState(this._config);

        // order is important for calculating available space for elements
        this._setLeftAxisData();
        this._setBottomAxisData();

        this._alignBottomAxis();
        this._alignLeftAxis();

        this._drawHeatScale();
        this._drawCells();
    }

    private _resizeChart() {
        var $this = this;

        if (this._service.actionsTimeout) {
            return;
        }

        this._service.actionsTimeout = window.setTimeout(function () {
            $this._alignBottomAxis();
            $this._alignLeftAxis();

            $this._drawHeatScale();
            $this._drawCells();
            $this._service.actionsTimeout = null;
        }, 30);
    }

    private _setLeftAxisData() {
        var leftAxisLabels = this._config.data.axisLeft.data.map(function(tick: any) {return tick.label});
        var axisTextStyles = this._baseConfigs.textStyles.axis;
        var leftAxis = d3.select('.left-axis');

        if (leftAxis.empty()) {
            leftAxis = <any>this._getSVG()
                .append('g')
                .classed('left-axis', true)
                .style(axisTextStyles)
            ;
        } else {
            leftAxis.text('');
        }

        // set axis ticks
        for (var i = 0; i < leftAxisLabels.length; i++) {
            leftAxis
                .append('text')
                .classed('axis-label', true)
                .attr('text-anchor', 'end')
                .text(leftAxisLabels[leftAxisLabels.length - 1 - i])
            ;
        }
    }

    private _setBottomAxisData() {
        var bottomAxisLabels = this._config.data.axisBottom.data.map(function(tick: any) {return tick.label});
        var axisTextStyles = this._baseConfigs.textStyles.axis;
        var bottomAxis = d3.select('.bottom-axis');

        if (bottomAxis.empty()) {
            bottomAxis = <any>this._getSVG()
                .append('g')
                .classed('bottom-axis', true)
                .style(axisTextStyles)
            ;
        } else {
            bottomAxis.text('');
        }

        // set axis ticks
        for (var i = 0; i < bottomAxisLabels.length; i++) {
            bottomAxis
                .append('text')
                .classed('axis-label', true)
                .attr('text-anchor', 'middle')
                .text(bottomAxisLabels[i])
            ;
        }
    }

    private _alignBottomAxis() {
        var $this = this;
        var bottomAxis = this._getSVG().select('.bottom-axis');
        var marginToChart = this._baseConfigs.axes.chartMargin;
        var marginToBoundary = this._baseConfigs.axes.boundaryMargin;
        var heatScaleFullWidth = this._baseConfigs.heatScale.width + marginToChart + marginToBoundary;
        var cellBorderWidth = this._baseConfigs.cell.borderWidth;
        var svgHeight = this._getSVGSize().height;
        var svgWidth = this._getSVGSize().width;
        var leftAxisElem = this._getSizeBySelector('.left-axis');
        var leftAxisFullWidth = (leftAxisElem ? leftAxisElem.width : 0) + marginToChart + marginToBoundary;
        var horizontalAxisAvailableWidth = svgWidth - leftAxisFullWidth - heatScaleFullWidth;
        var cellWidth = horizontalAxisAvailableWidth / this._config.data.axisBottom.data.length;
        var maxTextWidth = 0;
        var textSize = parseFloat(this._baseConfigs.textStyles.axis['font-size']);
        var rotateAngle!: number;
        var cos!: number;
        var tan!: number;
        var sin!: number;
        var yPos!: number;
        var xPosShift!: number;
        var leftFreeSpace!: number;

        bottomAxis
            .selectAll('text')
            .each(function(this: SVGTextElement) {
                const textRectSize = this.getBoundingClientRect();
                const textRectWidth = textRectSize.right - textRectSize.left;

                if (textRectWidth >= maxTextWidth) {
                    maxTextWidth = textRectWidth;
                }
            })
        ;

        bottomAxis.attr('transform', 'translate(' + this._round(leftAxisFullWidth) + ', ' + this._round(svgHeight - marginToBoundary) + ')');

        if (maxTextWidth >= cellWidth) {
            leftFreeSpace = leftAxisFullWidth + cellWidth / 2;

            if (maxTextWidth < leftFreeSpace) {
                rotateAngle = -25;
                cos = Math.cos(rotateAngle * Math.PI / 180);
                sin = Math.sqrt(1 - Math.pow(cos, 2));
                tan = sin / cos;
                yPos = this._round(-maxTextWidth * tan);
            } else {
                cos = leftFreeSpace / maxTextWidth;
                sin = Math.sqrt(1 - Math.pow(cos, 2));
                tan = sin / cos;
                rotateAngle = this._round(-(Math.acos(cos) * 180 / Math.PI));
                yPos = this._round(-leftFreeSpace * tan);
            }

            xPosShift = textSize * sin / 2 - cellBorderWidth;

            bottomAxis
                .selectAll('text')
                .each(function(this: SVGTextElement, d: any, i: number) {
                    d3.select(this)
                        .attr('transform', 'rotate(' + rotateAngle + ', ' + $this._round(cellWidth * (i + 0.5) + xPosShift) + ', ' + yPos + ')')
                        .attr('text-anchor', 'end')
                        .attr('x', $this._round(cellWidth * (i + 0.5) + xPosShift))
                        .attr('y', yPos)
                    ;
                })
            ;

        } else {
            bottomAxis
                .selectAll('text')
                .each(function(this: SVGTextElement, d: any, i: number) {
                    d3.select(this)
                        .attr('x', $this._round(cellWidth * (i + 0.5)))
                        .attr('y', 0)
                })
            ;
        }
    }

    private _alignLeftAxis() {
        var $this = this;
        var leftAxis = this._getSVG().select('.left-axis');
        var halfTextSize = parseFloat(this._baseConfigs.textStyles.axis['font-size']) / 2;
        var marginToBoundary = this._baseConfigs.axes.boundaryMargin;
        var marginToChart = this._baseConfigs.axes.chartMargin;
        var leftAxisElement = <any>this._getSizeBySelector('.left-axis');
        var leftAxisFullWidth = leftAxisElement.width + marginToChart + marginToBoundary;
        var svgHeight = this._getSVGSize().height;
        var bottomAxisElement = <any>this._getSizeBySelector('.bottom-axis');
        var bottomAxisFullHeight = bottomAxisElement.height + marginToChart + marginToBoundary;
        var verticalAvailableHeight = svgHeight - bottomAxisFullHeight;
        var cellHeight = verticalAvailableHeight / this._config.data.axisLeft.data.length;
        var cellBorderWidth = this._baseConfigs.cell.borderWidth;

        leftAxis
            .attr('transform', 'translate(' + this._round(leftAxisFullWidth - marginToChart) + ', 0)')
            .selectAll('text')
            .each(function(this: SVGTextElement, d: any, i: number) {
                d3.select(this)
                    .attr('y', $this._round(cellHeight * (i + 0.5) + halfTextSize - cellBorderWidth))
            })
        ;
    }

    private _drawHeatScale() {
        var heatScale = d3.select('.heat-scale');
        var defs = this._getSVG().select('defs');
        var marginToChart = this._baseConfigs.axes.chartMargin;
        var marginToBoundary = this._baseConfigs.axes.boundaryMargin;
        var svgHeight = this._getSVGSize().height;
        var bottomAxisElement = <any>this._getSizeBySelector('.bottom-axis');
        var bottomAxisFullHeight = bottomAxisElement.height + marginToChart + marginToBoundary;
        var verticalAvailableHeight = svgHeight - bottomAxisFullHeight;
        var svgWidth = this._getSVGSize().width;
        var heatScaleFullWidth = this._baseConfigs.heatScale.width + marginToChart + marginToBoundary;
        var leftAxisElement = <any>this._getSizeBySelector('.left-axis');
        var leftAxisFullWidth = leftAxisElement.width + marginToChart + marginToBoundary;
        var horizontalAxisAvailableWidth = svgWidth - leftAxisFullWidth - heatScaleFullWidth;
        var translateLeft = leftAxisFullWidth + horizontalAxisAvailableWidth + marginToChart;
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;
        var gradientConfig = this._config.options.scaleConfig[selectedProperty].gradient;
        var gradient, heatScaleRect;

        if (heatScale.empty()) {
            heatScale = <any>this._getSVG()
                .append('g')
                .classed('heat-scale', true)
                .style(this._baseConfigs.textStyles.heatScale)
            ;
        } else {
            heatScale.selectAll('*').remove();
        }

        if (!defs.empty()) {
            defs.remove();
        }

        defs = this._getSVG().append('defs');
        gradient = defs
            .append('linearGradient')
            .attr('id', 'heatscale-gradient')
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', 0)
            .attr('y2', '100%')
        ;

        for (var i = gradientConfig.length - 1; i >= 0; i--) {
            gradient.append('stop')
                .attr('offset', (100 - gradientConfig[i].offset) + '%')
                .attr('stop-color', gradientConfig[i].color)
            ;
        }

        heatScaleRect = heatScale
            .attr('transform', 'translate(' + this._round(translateLeft) + ', 0)')
            .append('rect')
            .style('fill', 'url(#heatscale-gradient)')
            .attr('x', 0)
            .attr('y', 0)
            .attr('rx', this._baseConfigs.heatScale.radius)
            .attr('ry', this._baseConfigs.heatScale.radius)
            .attr('width', this._baseConfigs.heatScale.radius * 2)
            .attr('height', this._round(verticalAvailableHeight))
        ;

        if (!this._config.options.scaleConfig[selectedProperty].dynamicGradient) {
            defs
                .append('clipPath')
                .attr('id', 'scale-clip')
                .append('rect')
                .attr('id', 'scale-clip-rect')
                .attr('x', 0)
                .attr('y', 0)
                .attr('width', this._baseConfigs.heatScale.radius * 2)
                .attr('height', this._round(verticalAvailableHeight))
            ;

            heatScaleRect.attr('clip-path', 'url(#scale-clip)');
        }

        this._addHeatLabels(verticalAvailableHeight);
        this._addSliderControls(verticalAvailableHeight);
    }

    private _redrawScaleRect(topY: number, bottomY: number) {
        var heatScaleRect = d3.select('.heat-scale rect');
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;

        if (!this._config.options.scaleConfig[selectedProperty].dynamicGradient) {
            d3.select('#scale-clip-rect')
                .attr('y', topY + this._baseConfigs.heatScale.radius)
                .attr('height', bottomY - topY - this._baseConfigs.heatScale.radius * 2)
            ;
        } else {
            heatScaleRect
                .style('fill', 'url(#heatscale-gradient)')
                .attr('x', 0)
                .attr('y', topY)
                .attr('height', bottomY - topY)
            ;
        }
    }

    private _addHeatLabels(height: number) {
        var heatScale = d3.select('.heat-scale');
        var textHeight = parseFloat(this._baseConfigs.textStyles.heatScale['font-size']);
        var heatScaleRadius = this._baseConfigs.heatScale.radius;
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;
        var scaleConfig = this._config.options.scaleConfig[selectedProperty];
        var minValue = this._service.getMinValue();
        var maxValue = this._service.getMaxValue();
        var step = this._service.getStep();
        var textY, labelY, precision, currentValue, heatLabelText, heatScaleLabels;

        if (heatScale.empty()) {
            return;
        }

        heatScaleLabels = heatScale
            .append('g')
            .classed('heat-scale-labels', true)
        ;

        this._attachHeatScaleDropdown();

        // set labels precision from scale config's step value
        precision = String(step).indexOf('.') > 0 ? String(step).split('.')[1].length : 0;

        for (currentValue = minValue; currentValue < maxValue + step; currentValue += step) {

            if (currentValue >= maxValue && currentValue < maxValue + step) {
                textY = textHeight + 1;
                labelY = heatScaleRadius + 1;
                heatLabelText = Math.min(currentValue, maxValue).toFixed(precision);
            } else if (currentValue === minValue) {
                textY = this._round(height - 0.5 * textHeight + 1);
                labelY = this._round(height - heatScaleRadius);
                heatLabelText = currentValue.toFixed(precision);
            } else {
                textY = this._round(height - ((currentValue - minValue) / (maxValue - minValue)) * (height - heatScaleRadius * 2 - 2) - textHeight / 2 + 1);
                labelY = this._round(height - ((currentValue - minValue) / (maxValue - minValue)) * (height - heatScaleRadius * 2 - 2) - heatScaleRadius);
                heatLabelText = currentValue.toFixed(precision);
            }

            heatScaleLabels
                .append('line')
                .classed('heatscale-division-line', true)
                .attr('x1', 20)
                .attr('y1', labelY)
                .attr('x2', 30)
                .attr('y2', labelY)
                .attr('stroke', this._baseConfigs.heatScale.delColor)
                .attr('stroke-width', 1)
            ;

            if (scaleConfig.units) {
                heatLabelText += scaleConfig.units;
            }

            heatScaleLabels
                .append('text')
                .classed('heatscale-division-text', true)
                .attr('x', 35)
                .attr('y', textY)
                .text(heatLabelText)
            ;
        }

        heatScaleLabels
            .append('line')
            .classed('heatscale-divisions', true)
            .attr('x1', 20)
            .attr('y1', heatScaleRadius)
            .attr('x2', 20)
            .attr('y2', height - heatScaleRadius)
            .attr('stroke', this._baseConfigs.heatScale.delColor)
            .attr('stroke-width', 1)
        ;
    }

    private _attachHeatScaleDropdown() {
        var $this = this;
        var heatScaleLabels = d3.select('.heat-scale-labels');
        var stateService = this._service;
        var analyseProps = stateService.getAnalyseProps();
        var dropdownWidth = this._baseConfigs.dropdown.width;
        var heatScaleDropdown = d3.select('.heat-scale-dropdown');
        var dropdownItem;

        if (!heatScaleDropdown.empty()) {
            heatScaleDropdown.remove();
        }

        heatScaleDropdown = d3.select('body')
            .append('div')
            .classed('heat-scale-dropdown dropdown-wrap dropdown-scope dropdown-closed', true)
            .style({
                'opacity': 0,
                'display': 'none',
                'position': 'fixed',
                'width': dropdownWidth + 'px',
                'min-width': dropdownWidth + 'px'
            })
        ;

        for (var i = 0; i < analyseProps.length; i++) {
            dropdownItem = heatScaleDropdown
                .append('div')
                .classed('dropdown-collection', true)
                .append('div')
                .classed('dropdown-item', true)
                .text(analyseProps[i].label)
                .attr('data-analysed-prop-value', analyseProps[i].value)
            ;

            dropdownItem.on('click', function(this: HTMLElement) {
                var el = d3.select(this);

                if (el.classed('selected')) {
                    (<any>d3.event).stopPropagation();

                    return;
                }

                stateService.selectAnalyseProp(el.attr('data-analysed-prop-value'));
                $this._selectActiveAnalyseItem();
                $this._drawChart();
            });
        }

        this._selectActiveAnalyseItem();

        heatScaleLabels.on('click', function () {
            (<any>d3.event).stopPropagation();

            $this._toggleAnalyseDropdown();
        });

        heatScaleLabels.on('mouseenter', function(this: HTMLElement) {
            d3.select(this)
                .style({
                    'fill': $this._config.options.primaryColor || $this._baseConfigs.textStyles.common.fill,
                    'cursor': 'pointer'
                })
            ;
        });

        heatScaleLabels.on('mouseleave', function(this: HTMLElement) {
            (<any>d3.select(this))
                .attr('style', null)
            ;
        });
    }

    private _selectActiveAnalyseItem() {
        var selectedItemClass = 'selected cursor-default';
        var heatScaleDropdown = d3.select('.heat-scale-dropdown');
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;

        heatScaleDropdown
            .selectAll('.dropdown-item')
            .each(function(this: HTMLElement) {
                var el = d3.select(this);

                el.classed(
                    selectedItemClass,
                    el.attr('data-analysed-prop-value') === selectedProperty
                );
            })
        ;
    }

    private _toggleAnalyseDropdown() {
        var heatScaleDropdown = d3.select('.heat-scale-dropdown');
        var screenPadding = 20;
        var dropdownRect = (<any>heatScaleDropdown.node()).getBoundingClientRect();
        var dropdownWidth = this._baseConfigs.dropdown.width;
        var dropdownHeight = dropdownRect.bottom - dropdownRect.top;
        var bodyRect = (<any>d3.select('body').node()).getBoundingClientRect();
        var top, left;

        if (heatScaleDropdown.classed('dropdown-closed')) {
            if ((<any>d3.event).x + dropdownWidth + screenPadding > bodyRect.right) {
                left = bodyRect.right - screenPadding - dropdownWidth;
            } else {
                left = (<any>d3.event).x;
            }

            if ((<any>d3.event).y + dropdownHeight + screenPadding > bodyRect.bottom) {
                top = bodyRect.bottom - screenPadding - dropdownHeight;
            } else {
                top = (<any>d3.event).y;
            }

            heatScaleDropdown
                .classed('dropdown-closed', false)
                .style({
                    'display': 'inline-block',
                    'top': top + 'px',
                    'left': left + 'px'
                })
                .transition()
                .duration(125)
                .style('opacity', 1)
            ;
        } else {
            this._closeAnalyseDropdown();
        }
    }

    private _closeAnalyseDropdown() {
        var heatScaleDropdown = d3.select('.heat-scale-dropdown');

        heatScaleDropdown
            .classed('dropdown-closed', true)
            .transition()
            .duration(125)
            .style('opacity', 0)
            .style('display', 'none')
        ;
    }

    private _addSliderControls(height: number) {
        var $this = this;
        var heatScale = d3.select('.heat-scale');
        var heatScaleRadius = this._baseConfigs.heatScale.radius;
        var topYPos = heatScaleRadius;
        var bottomYPos = height - heatScaleRadius;
        var scaleHeight = height - heatScaleRadius * 2;
        var minValue = $this._service.getMinValue();
        var maxValue = $this._service.getMaxValue();
        var bottomValue = $this._service.getMinValue();
        var topValue = $this._service.getMaxValue();
        var minDistance = 10;
        var topSlider, bottomSlider;

        function updateHeatmapOnSlide() {
            $this._redrawScaleRect(
                topYPos - heatScaleRadius,
                bottomYPos + heatScaleRadius
            );

            if ($this._config.options.scaleConfig[$this._service.getSelectedAnalyseProperty().value].dynamicGradient) {
                $this._service.setColorScale(bottomValue, topValue);
            }

            $this._redrawCells(topValue, bottomValue);
        }

        topSlider = heatScale
            .append('g')
            .classed('heatscale-slider', true)
            .attr('transform', 'translate(0,' + topYPos + ')')
            .style('cursor', '-webkit-grab')
            .style('cursor', 'grab')
            .call(
                d3.behavior.drag()
                    .on('dragstart', function(this: SVGGElement) {
                        d3.select(this)
                            .style('cursor', '-webkit-grabbing')
                            .style('cursor', 'grabbing')
                        ;
                    })
                    .on('dragend', function(this: SVGGElement) {
                        d3.select(this)
                            .style('cursor', '-webkit-grab')
                            .style('cursor', 'grab')
                        ;

                        $this._hideScaleTooltip();
                    })
                    .on('drag', function (this: SVGGElement) {
                        topYPos = (<any>d3.event).y;

                        if (topYPos > bottomYPos - minDistance) {
                            topYPos = bottomYPos - minDistance;
                        }

                        if (topYPos < heatScaleRadius) {
                            topYPos = heatScaleRadius
                        }

                        topValue = maxValue - (topYPos - heatScaleRadius) / scaleHeight * (maxValue - minValue);

                        updateHeatmapOnSlide();
                        $this._updateScaleTooltip(topValue, $this._round(topYPos));

                        d3.select(this)
                            .attr('transform', 'translate(0,' + $this._round(topYPos) + ')')
                        ;
                    })
            )
        ;

        bottomSlider = heatScale
            .append('g')
            .classed('heatscale-slider', true)
            .attr('transform', 'translate(0,' + bottomYPos + ')')
            .style('cursor', '-webkit-grab')
            .style('cursor', 'grab')
            .call(
                d3.behavior.drag()
                    .on('dragstart', function(this: SVGGElement) {
                        d3.select(this)
                            .style('cursor', '-webkit-grabbing')
                            .style('cursor', 'grabbing')
                        ;
                    })
                    .on('dragend', function(this: SVGGElement) {
                        d3.select(this)
                            .style('cursor', '-webkit-grab')
                            .style('cursor', 'grab')
                        ;

                        $this._hideScaleTooltip();
                    })
                    .on('drag', function(this: SVGGElement) {
                        bottomYPos = (<any>d3.event).y;

                        if (bottomYPos < topYPos + minDistance) {
                            bottomYPos = topYPos + minDistance;
                        }

                        if (bottomYPos > height - heatScaleRadius) {
                            bottomYPos = height - heatScaleRadius
                        }

                        bottomValue = maxValue - (bottomYPos - heatScaleRadius) / scaleHeight * (maxValue - minValue);

                        updateHeatmapOnSlide();
                        $this._updateScaleTooltip(bottomValue, $this._round(bottomYPos));

                        d3.select(this)
                            .attr('transform', 'translate(0,' + $this._round(bottomYPos) + ')')
                        ;
                    })
            )
        ;

        this._addSliderContent(topSlider, 1);
        this._addSliderContent(bottomSlider, 0);
        this._addScaleTooltip();
    }

    private _addSliderContent(slider: any, offset: number) {
        slider
            .append('circle')
            .attr('cx', this._baseConfigs.heatScale.radius)
            .attr('cy', offset)
            .attr('r', this._baseConfigs.heatScale.radius)
            .attr('fill', '#ffffff')
            .attr('stroke', this._baseConfigs.heatScale.delColor)
            .attr('stroke-width', 1)
        ;
    }

    private _addScaleTooltip() {
        var tooltipWidth = this._baseConfigs.heatScale.width - 25;
        var tooltipHeight = 22;
        var tooltip = d3.select('.heat-scale')
            .append('svg')
            .attr('width', tooltipWidth)
            .attr('height', tooltipHeight)
            .attr('y', -tooltipHeight / 2)
            .attr('x', 25)
            .classed('heatscale-tooltip', true)
            .style('opacity', 0)
            .style('fill', '#ffffff')
        ;

        tooltip
            .append('rect')
            .classed('heatscale-tooltip-rect', true)
            .style('fill', this._baseConfigs.textStyles.common.fill)
            .attr('x', 0)
            .attr('y', 0)
            .attr('rx', 4)
            .attr('ry', 4)
            .attr('width', tooltipWidth)
            .attr('height', tooltipHeight)
        ;

        tooltip
            .append('text')
            .classed('heatscale-tooltip-text', true)
            .style('font-size', '12px')
            .attr('y', '50%')
            .attr('x', '50%')
            .attr('alignment-baseline', 'middle')
            .attr('text-anchor', 'middle')
        ;
    }

    private _hideScaleTooltip() {
        d3.select('.heatscale-tooltip')
            .transition()
            .duration(200)
            .style('opacity', 0)
        ;
    }

    private _updateScaleTooltip(label: string | number, position: number) {
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;
        var scaleConfig = this._config.options.scaleConfig[selectedProperty];
        var heatLabelText;

        heatLabelText = this._round(label, 3);

        if (scaleConfig.units) {
            heatLabelText += scaleConfig.units;
        }

        d3.select('.heatscale-tooltip')
            .style('opacity', 1)
            .attr('y', position - 11)
        ;

        d3.select('.heatscale-tooltip-text').text(heatLabelText);
    }

    private _drawCells() {
        var cells = d3.select('.cells');
        var marginToChart = this._baseConfigs.axes.chartMargin;
        var marginToBoundary = this._baseConfigs.axes.boundaryMargin;
        var svgWidth = this._getSVGSize().width;
        var svgHeight = this._getSVGSize().height;
        var heatScaleFullWidth = this._baseConfigs.heatScale.width + marginToChart + marginToBoundary;
        var leftAxisLabels = this._config.data.axisLeft.data.map(function(tick: any) {return tick.label});
        var bottomAxisLabels = this._config.data.axisBottom.data.map(function(tick: any) {return tick.label});
        var leftAxisFullWidth = (<any>this._getSizeBySelector('.left-axis')).width + marginToChart + marginToBoundary;
        var bottomAxisFullHeight = (<any>this._getSizeBySelector('.bottom-axis')).height + marginToChart + marginToBoundary;
        var cellWidth = (svgWidth - leftAxisFullWidth - heatScaleFullWidth) / bottomAxisLabels.length;
        var cellHeight = (svgHeight - bottomAxisFullHeight) / leftAxisLabels.length;
        var cellsConfig = this._config.data.cells;
        var cellTextParams = this._getCellTextParams(cellWidth, cellHeight);
        var indexInLeftAxis, indexInBottomAxis;

        if (cells.empty()) {
            this._getSVG()
                .append('g')
                .classed('cells', true)
                .style({
                    'cursor': 'pointer',
                    'font-size': cellTextParams.size + 'px'
                })
                .attr('transform', 'translate(' + this._round(leftAxisFullWidth) + ', 0)')
            ;
        } else {
            cells.selectAll('*').remove();

            cells
                .style('font-size', cellTextParams.size + 'px')
                .attr('transform', 'translate(' + this._round(leftAxisFullWidth) + ', 0)')
            ;
        }

        // set cells and heatmap bricks
        for (var i = 0; i < cellsConfig.length; i++) {
            indexInLeftAxis = leftAxisLabels.indexOf(cellsConfig[i].y);
            indexInBottomAxis = bottomAxisLabels.indexOf(cellsConfig[i].x);

            // insert cell content - rectangle, line and text labels
            this._addCell({
                config: cellsConfig[i][this._service.getSelectedAnalyseProperty().value],
                width: cellWidth,
                height: cellHeight,
                xIndex: indexInBottomAxis,
                yIndex: leftAxisLabels.length - 1 - indexInLeftAxis,
                textParams: cellTextParams,
                eventHandlers: cellsConfig[i].eventHandlers
            });
        }
    }

    /**
     * config - cell config
     * width - cell width
     * height - cell height
     * xIndex - index in bottom axis starting from left
     * yIndex - index in left axis starting from bottom
     * textParams - calculated text params - size and margins
     */
    private _addCell(options: any) {
        var cells = this._getSVG().select('.cells');
        var cellStyle = this._baseConfigs.cell;
        var cellTextColor = this._baseConfigs.textStyles.common.fill;
        var cellBorderWidth = this._baseConfigs.cell.borderWidth;
        var colorScale = this._service.getColorScale();
        var cell, cellInfo, cellColor;

        if (options.xIndex === -1 || options.yIndex === -1) {
            return;
        }

        if (options.width < 0) {
            options.width = 0;
        }

        if (options.height < 0) {
            options.height = 0;
        }

        cell = cells
            .append('g')
            .datum(options.config ? options.config.conjunction : null)
            .classed('cell', true)
            .attr('transform', 'translate(' + this._round(options.xIndex * options.width) + ', ' + this._round(options.yIndex * options.height) + ')')
        ;

        cellColor = options.config
            ? colorScale(options.config.conjunction)
            : cellStyle.initialColor;

        // insert rectangle in the grid
        cell
            .append('rect')
            .classed('cell-rect', true)
            .attr('rx', 4)
            .attr('ry', 4)
            .attr('width', this._round(options.width))
            .attr('height', this._round(options.height))
            .attr('stroke', 'rgb(255,255,255)')
            .attr('stroke-width', cellBorderWidth)
            .style('fill', cellStyle.initialColor)
            .transition()
            .duration(750)
            .style('fill', cellColor)
        ;

        if (options.config) {
            cellInfo = cell
                .append('g')
                .classed('cell-info', true)
                .style('opacity', 0)
            ;

            this._insertCellLine(cellInfo, options.width, options.height); //, cellTextColor);

            this._insertCellText(cellInfo, {
                value: options.config.xValue,
                xPos: options.width - options.textParams.xMargin - cellBorderWidth,
                yPos: options.height - options.textParams.yMargin - cellBorderWidth,
                alignToEnd: true,
                color: cellTextColor
            });

            this._insertCellText(cellInfo, {
                value: options.config.yValue,
                xPos: options.textParams.xMargin + cellBorderWidth,
                yPos: options.textParams.size,
                alignToEnd: false,
                color: cellTextColor
            });
        } else {
            cell.style('pointer-events', 'none');
        }

        if (!options.eventHandlers) {
            cell.style('cursor', 'default');
        }

        this._attachCellHandlers(cell, options.eventHandlers);
    }

    private _insertCellLine(cell: any, cellWidth: number, cellHeight: number) {
        var cellBorderWidth = this._baseConfigs.cell.borderWidth;

        cell
            .append('line')
            .classed('cell-line', true)
            .attr('x1', 1)
            .attr('y1', this._round(cellHeight - cellBorderWidth))
            .attr('x2', this._round(cellWidth - cellBorderWidth))
            .attr('y2', 1)
            .attr('stroke', 'rgb(255,255,255)')
            .attr('stroke-width', cellBorderWidth)
        ;
    }

    private _insertCellText(cell: any, textConfig: any) {
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;
        var scaleConfig = this._config.options.scaleConfig[selectedProperty];
        var cellTextLabel, cellText;

        cellTextLabel = Math.round(textConfig.value * 1000) / 1000;

        if (scaleConfig.units) {
            cellTextLabel += scaleConfig.units;
        }

        cellText = cell
            .append('text')
            .classed('cell-text', true)
            .style('fill', textConfig.color)
            .attr('x', this._round(textConfig.xPos))
            .attr('y', this._round(textConfig.yPos))
            .text(cellTextLabel)
        ;

        if (textConfig.alignToEnd) {
            cellText.attr('text-anchor', 'end');
        } else {
            cellText.attr('text-anchor', 'start');
        }
    }

    /**
     * show/hide cells depending on the selected range in the scale slider
     * color palette of the all visible cell should stay in the same gradient range
     *
     * topSlideValue - average cell value, selected by top slider control
     * bottomSlideValue - average cell value, selected by bottom slider control
     */
    private _redrawCells(topSlideValue: number, bottomSlideValue: number) {
        var initialColor = this._baseConfigs.cell.initialColor;
        var colorScale = this._service.getColorScale();
        var maxValueInRange = bottomSlideValue, minValueInRange = topSlideValue;


        this._getSVG()
            .selectAll('.cell')
            .each(function(avgValue: any) {
                var isInHeatRange = avgValue <= topSlideValue && avgValue >= bottomSlideValue;

                if (avgValue < minValueInRange && isInHeatRange) {
                    minValueInRange = avgValue;
                }

                if (avgValue > maxValueInRange && isInHeatRange) {
                    maxValueInRange = avgValue;
                }
            })
        ;

        this._getSVG()
            .selectAll('.cell')
            .each(function(this: HTMLElement, avgValue: number) {
                var cell = d3.select(this);
                var isInHeatRange = avgValue <= maxValueInRange && avgValue >= minValueInRange;
                var cellColor;

                cellColor = isInHeatRange ? colorScale(avgValue) : initialColor;

                cell.select('.cell-rect')
                    .transition()
                    .duration(200)
                    .style('fill', cellColor)
                ;

                cell.style('pointer-events', isInHeatRange ? 'all' : 'none');
            })
        ;
    }

    //=========================== DRAW CHART END =========================

    //=========================== HANDLERS ===============================

    private _attachCellHandlers(cell: any, eventHandlers: any) {
        if (cell.empty()) {
            return;
        }

        for (var defaultEventName in this._defaultCellHandlers) {
            if (!this._defaultCellHandlers.hasOwnProperty(defaultEventName)) continue;

            cell.on(defaultEventName, this._defaultCellHandlers[defaultEventName]);
        }

        for (var eventName in eventHandlers) {
            if (!eventHandlers.hasOwnProperty(eventName)) continue;

            cell.on(eventName, eventHandlers[eventName]);
        }
    }

    private _attachWindowHandlers() {
        for (var eventName in this._windowHandlers) {
            if (!this._windowHandlers.hasOwnProperty(eventName)) continue;

            window.addEventListener(eventName, this._windowHandlers[eventName]);
        }
    }

    private _detachWindowHandlers() {
        for (var eventName in this._windowHandlers) {
            if (!this._windowHandlers.hasOwnProperty(eventName)) continue;

            window.removeEventListener(eventName, this._windowHandlers[eventName]);
        }
    }

    //=========================== HANDLERS END ===========================

    //=========================== HELPERS ===============================

    private _getSVG() {
        return d3.select('#' + this._elementId + ' svg');
    }

    private _getSVGSize() {
        if (this._getSVG().empty()) {
            return { width: 0, height: 0 };
        }

        var boundingRect = (<SVGElement>this._getSVG().node()).getBoundingClientRect();
        var width = boundingRect.right - boundingRect.left;
        var height = boundingRect.bottom - boundingRect.top;

        return {
            width: width,
            height: height
        }
    }

    private _getSizeBySelector(selector: string) {
        var element = this._getSVG().select(selector);
        var width, height;

        if (!this._getSVG() || element.empty()) {
            return;
        }

        const elementSize = (<SVGElement>element.node()).getBoundingClientRect();

        width = elementSize.right - elementSize.left;
        height = elementSize.bottom - elementSize.top;

        return {
            width: width,
            height: height
        }
    }

    private _getCellTextParams(cellWidth: number, cellHeight: number) {
        var defs = this._getSVG().select('defs');
        var defaultSize = parseFloat(this._baseConfigs.textStyles.common['font-size']);
        var cellBorderWidth = this._baseConfigs.cell.borderWidth;
        var selectedProperty = this._service.getSelectedAnalyseProperty().value;
        var scaleConfig = this._config.options.scaleConfig[selectedProperty];
        var xMargin = 5, yMargin = 5;
        var textContainer, textEl, textElRect, textSize, sizeScale, tanA, tanB, defText;

        if (!cellWidth || !cellHeight) {
            return {
                size: defaultSize,
                xMargin: xMargin,
                yMargin: yMargin
            };
        }

        if (cellWidth / 20 < xMargin) {
            xMargin = Math.floor(cellWidth / 20);
        }

        if (cellHeight / 20 < yMargin) {
            yMargin = Math.floor(cellHeight / 20);
        }

        if (defs.empty()) {
            defs = this._getSVG().append('defs');
        }

        defText = this._round(scaleConfig.maxValue || '100', 3);

        if (scaleConfig.units) {
            defText += scaleConfig.units;
        }

        textEl = defs.select('text');

        if (!textEl.empty()) {
            textEl.text(defText);
        } else {
            textEl = defs
                .append('text')
                .text(defText)
            ;
        }

        textElRect = (<SVGTextElement>textEl.node()).getBoundingClientRect();
        textSize = {
            width: textElRect.right - textElRect.left,
            height: textElRect.bottom - textElRect.top
        };
        tanA = textSize.height / textSize.width;
        tanB = cellHeight / cellWidth;

        textContainer = {
            width: cellWidth * tanB / (tanA + tanB) - cellBorderWidth - 2 * xMargin,
            height: cellWidth * tanA * tanB / (tanA + tanB) - cellBorderWidth - 2 * yMargin
        };

        sizeScale = textSize.width / textContainer.width;

        return {
            size: sizeScale < 1 ? defaultSize : Math.floor(defaultSize / sizeScale),
            xMargin: xMargin,
            yMargin: yMargin
        };
    }

    private _round(value: number | string, precision?: number) {
        if (precision === undefined) {
            precision = 2;
        }

        if (typeof value === 'string') {
            return parseFloat((+value).toFixed(precision));
        }

        if (typeof value === 'number') {
            return parseFloat(value.toFixed(precision));
        }

        return value;
    }

    private _setError(errorMsg: string) {
        var heatmap = d3.select('#' + this._elementId);

        if (!this._getSVG().empty()) {
            this._getSVG().remove();
        }

        heatmap.append('div')
            .classed('info-data-doesnot-exist full-screen', true)
            .html(
                '<div class="alignment">\
                    <div class="alignment-cell text-md">\
                        <p class="text-center text-lg margin-bottom">\
                            <i class="fa fa-3x fa-lock"></i>\
                        </p>\
                        <span>' + errorMsg + '</span>\
                    </div>\
                </div>'
            );
    }
    //=========================== HELPERS END ===========================
}

/**
 * service to keep different calculated or current state params
 */
class StateService {
    private _chartConfig!: any;
    private _maxValue!: number;
    private _minValue!: number;
    private _step!: number;
    private _actionsTimeout!: number | null;
    private _colorScale!: any;
    private _analyseProps!: any;

    get actionsTimeout() {
        return this._actionsTimeout;
    }

    set actionsTimeout(timeout) {
        this._actionsTimeout = timeout;
    }

    public getMinValue() {
        return this._minValue;
    }

    public getMaxValue() {
        return this._maxValue;
    }

    public getStep() {
        return this._step;
    }

    public getColorScale() {
        return this._colorScale;
    }

    public setColorScale(minValue: number, maxValue: number) {
        var analysedProperty = this.getSelectedAnalyseProperty().value;
        var scalePropertyConfig = this._chartConfig.options.scaleConfig[analysedProperty];
        var domainArr = [], colorArr = [];

        for (var i = 0; i < scalePropertyConfig.gradient.length; i++) {
            domainArr.push(
                minValue + (maxValue - minValue) / 100 * scalePropertyConfig.gradient[i].offset
            );

            colorArr.push(scalePropertyConfig.gradient[i].color);
        }

        this._colorScale = d3.scale.linear()
            .domain(domainArr)
            .range(colorArr)
        ;
    }

    /**
     * reset all calculated values
     * at the end of all calculations minValue, maxValue and scale step should be defined event if they are not present in config
     */
    public resetState(chartConfig: any) {
        var analysedProperty;
        var scalePropertyConfig;

        if (
            !chartConfig
            || !chartConfig.options
            || !chartConfig.options.scaleConfig
            || !chartConfig.options.analyseProps
        ) {
            console.error('Chart scale config is missing or incorrect!');

            return;
        }

        this._chartConfig = chartConfig;
        this.setAnalyseProps(chartConfig.options.analyseProps);

        analysedProperty = this.getSelectedAnalyseProperty().value;
        scalePropertyConfig = chartConfig.options.scaleConfig[analysedProperty];

        if (scalePropertyConfig.divisions) {
            this._minValue = scalePropertyConfig.divisions.min;
            this._maxValue = scalePropertyConfig.divisions.max;
            this._step = scalePropertyConfig.divisions.step;
        } else {
            this._calculateScaleDivisions();
        }

        this.setColorScale(this._minValue, this._maxValue);
    }

    public setAnalyseProps(props: any) {
        this._analyseProps = props;
    }

    public getAnalyseProps() {
        return this._analyseProps;
    }

    public getSelectedAnalyseProperty() {
        if (!this._analyseProps) {
            return;
        }

        for (var i = 0; i < this._analyseProps.length; i++) {
            if (this._analyseProps[i].active) {
                return this._analyseProps[i];
            }
        }
    }

    public selectAnalyseProp(propValue: any) {
        for (var i = 0; i < this._analyseProps.length; i++) {
            this._analyseProps[i].active = this._analyseProps[i].value === propValue;
        }
    }

    /**
     * calculate scale divisions - min/max values and step - if they were not defined in the config
     */
    private _calculateScaleDivisions() {
        if (!this._chartConfig) {
            return;
        }

        var cells = this._chartConfig.data.cells;
        var analysedProperty = this.getSelectedAnalyseProperty().value;
        var minCellValue!: number, maxCellValue!: number;

        // amazing function
        function findSuitableStep(steps: Array<number>): number {
            var step = (maxCellValue - minCellValue) / 10;
            var closestStep, diff;

            if (step > steps[3]) {
                steps = steps.map(function(step){ return step * 10 });

                return findSuitableStep(steps);
            } else if (step < steps[0]) {
                steps = steps.map(function(step){ return step / 10 });

                return findSuitableStep(steps);
            } else {
                diff = step - steps[0];
                closestStep = steps[0];

                for (var i = 0; i < steps.length; i++) {
                    if (Math.abs(steps[i] - step) <= diff) {
                        diff = Math.abs(steps[i] - step);
                        closestStep = steps[i];
                    }
                }

                return closestStep;
            }
        }

        // find min and max among the cell values

        for (var i = 0; i < cells.length; i++) {
            if (!cells[i][analysedProperty]) {
                continue;
            }

            if (maxCellValue === undefined) {
                maxCellValue = cells[i][analysedProperty].conjunction;
            } else if (cells[i][analysedProperty].conjunction > maxCellValue) {
                maxCellValue = cells[i][analysedProperty].conjunction;
            }

            if (minCellValue === undefined) {
                minCellValue = cells[i][analysedProperty].conjunction;
            } else if (cells[i][analysedProperty].conjunction < minCellValue) {
                minCellValue = cells[i][analysedProperty].conjunction;
            }
        }

        // calculate step for the scale divisions
        if (minCellValue === maxCellValue) {
            minCellValue--;
            maxCellValue++;
        }

        this._step = findSuitableStep([1, 2, 5, 10]);
        this._maxValue = Math.ceil(maxCellValue / this._step) * this._step;
        this._minValue = Math.floor(minCellValue / this._step) * this._step;
    }
}

//====================================================================
//=========================== PUBLIC API =============================
//====================================================================

export const qsHeatmap = {

    /**
     * check if chart has been already created
     */
    chartCreated: function() {
        return !!_chartInstance;
    },

    /**
     * create new chart
     */
    create: function(elementId: string, chartConfig: any) {
        return _chartInstance = new Heatmap(elementId, chartConfig);
    },

    /**
     * update chart with new data
     */
    update: function(chartConfig: any) {
        if (!this.chartCreated()) {
            console.warn('Update attempt failed: no chart created yet.');

            return;
        }

        _chartInstance && _chartInstance.update(chartConfig);
    },

    /**
     * delete chart instance and all attached listeners
     */
    destruct: function() {
        if (!this.chartCreated()) {
            return;
        }

        _chartInstance && _chartInstance.destruct();
        _chartInstance = null;
    }
};
