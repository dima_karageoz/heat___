import * as config from './survey-text-chart-heatmap.json';
import {qsHeatmap} from './origin-heatmap';
import {HeatmapApi} from './heatmap/heatmapApi';
import {IAnalyseEvent} from './heatmap/components/heatmapHeatScale';

(() => {
    document.addEventListener('DOMContentLoaded', () => {

        const hostElement = document.getElementById('heatmap-host');

        const chartConfig = {
            data: (<any>config).default,
            options: {
                analyseProps: [
                    {
                        label: 'Population',
                        value: 'frequency',
                        active: true
                    },
                    {
                        label: 'Sentiment',
                        value: 'sentiment',
                        active: false
                    }
                ],
                scaleConfig: {
                    frequency: {
                        dynamicGradient: true,
                        units: <any>undefined,
                        gradient: [
                            {
                                offset: 0,
                                color: '#ffede5'
                            },
                            {
                                offset: 100,
                                color: '#f54900'
                            }
                        ]
                    },
                    sentiment: {
                        dynamicGradient: true,
                        units: <any>undefined,
                        gradient: [
                            {
                                offset: 0,
                                color: '#9591ff'
                            },
                            {
                                offset: 100,
                                color: '#c343f5'
                            }
                        ]
                    }
                },
                primaryColor: '#f05f72'
            }
        };

        if (hostElement && config) {
            const heatmap = new HeatmapApi();
            // const heatmap = qsHeatmap;

            heatmap.create('heatmap-host', chartConfig);

            heatmap.addAnaliseHandler((event: IAnalyseEvent) => {
                const [firstProp, secondProp] = event.analyseProps;

                if (firstProp.active) {
                    event.selectAnalyseProperty(secondProp.value);
                    alert(secondProp.label + ' property selected');
                } else {
                    event.selectAnalyseProperty(firstProp.value);
                    alert(firstProp.label + ' property selected');
                }
            });
        }}
    );
})();
