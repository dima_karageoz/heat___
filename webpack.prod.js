const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'production',

    entry: {
        app: './src/heatmap/heatmapApi.ts'
    },

    optimization: {
        minimize: true
    }
});
