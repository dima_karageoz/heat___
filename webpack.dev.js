const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
    mode: 'development',

    entry: {
        app: './src/index.ts'
    },

    devtool: 'inline-source-map',

    devServer: {
        contentBase: './dist'
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            title: "Heatmap Dev"
        })
    ]
});
